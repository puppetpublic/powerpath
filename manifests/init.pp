#
# Red-Hat-specific class to provide the PowerPath package, used for SAN
# redundancy.

class powerpath {
    case $operatingsystem {
        'redhat': {
            $license = 'B2PZ-MB4E-AF42-Q6K3-MM9C-Y2WM'
            $file = '/etc/emcp_registration'

            package {
                'EMCpower.LINUX': ensure => present;
                'lun_scan':       ensure => present;
            }
            exec { 'install powerpath license':
                command => "/sbin/emcpreg -f $file -a $license",
                require => Package['EMCpower.LINUX'],
                creates => $file,
            }
        }
    }
}